import torch
import torchvision.transforms as tvt
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10

from pytorch_ood.dataset.img import Textures
from pytorch_ood.detector import EnergyBased
from pytorch_ood.model import WideResNet
from pytorch_ood.utils import OODMetrics, ToUnknown

torch.manual_seed(123)
device = "cuda:0"

trans = tvt.Compose([tvt.Resize(size=(32, 32)), tvt.ToTensor()])

# setup data
dataset_train = CIFAR10(root="data", train=True, download=True, transform=trans)
dataset_in_test = CIFAR10(root="data", train=False, transform=trans)

# treat samples from this dataset as OOD
dataset_out_test = Textures(
    root="data", download=True, transform=trans, target_transform=ToUnknown()
)

# create data loaders
train_loader = DataLoader(dataset_train, batch_size=64, shuffle=True)
test_loader = DataLoader(dataset_in_test + dataset_out_test, batch_size=64)

# Stage 1: Create DNN
model = WideResNet(num_classes=10, pretrained="cifar10-pt").to(device)

# Stage 2: Create OOD detector
# Fitting is not required in this case
energy = EnergyBased(model)

# Stage 3: Evaluate Detectors
metrics_energy = OODMetrics()
model.eval()

with torch.no_grad():
    for n, batch in enumerate(test_loader):
        x, y = batch
        x = x.to(device)
        metrics_energy.update(energy(x), y)

print(metrics_energy.compute())
# result: {'AUROC': 0.738550066947937, 'AUPR-IN': 0.6711145043373108, 'AUPR-OUT': 0.7876706123352051, 'ACC95TPR': 0.42416879534721375, 'FPR95TPR': 0.8723999857902527}
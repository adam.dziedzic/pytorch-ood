from __future__ import print_function
import numpy as np
import argparse
import os
import random

import torch
import torch.nn.parallel
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

import dataset_utils
from pytorch_ood.detector import EnergyBased
from pytorch_ood.detector import Mahalanobis
from pytorch_ood.detector import MaxLogit
from pytorch_ood.detector import MCD
from pytorch_ood.detector import ODIN
from pytorch_ood.detector import MaxSoftmax

from pytorch_ood.utils import OODMetrics

model_names = ("vgg16", "vgg16_bn")

parser = argparse.ArgumentParser(
    description='Selective Classification for Self-Adaptive Training')
parser.add_argument('-d', '--dataset',
                    # default='cifar10',
                    # default='svhn',
                    defatul='catsdogs',
                    type=str,
                    choices=['cifar10', 'svhn', 'catsdogs'])
parser.add_argument('--data_path', type=str, default='/home/nicolas/data/')
parser.add_argument('--detector', type=str, default='energy')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
# Training
parser.add_argument('-t', '--train', dest='evaluate', action='store_true',
                    help='train the model. When evaluate is true, training is ignored and trained models are loaded.')
parser.add_argument('--epochs', default=300, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--train-batch', default=128, type=int, metavar='N',
                    help='train batchsize')
parser.add_argument('--test-batch', default=200, type=int, metavar='N',
                    help='test batchsize')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--schedule', type=int, nargs='+',
                    default=[25, 50, 75, 100, 125, 150, 175, 200, 225, 250,
                             275],
                    help='Multiply learning rate by gamma at the scheduled epochs (default: 25,50,75,100,125,150,175,200,225,250,275)')
parser.add_argument('--gamma', type=float, default=0.5,
                    help='LR is multiplied by gamma on schedule (default: 0.5)')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--sat-momentum', default=0.9, type=float,
                    help='momentum for sat')
parser.add_argument('--weight-decay', '--wd', default=5e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('-o', '--rewards', dest='rewards', type=float, nargs='+',
                    default=[2.2],
                    metavar='o',
                    help='The reward o for a correct prediction; Abstention has a reward of 1. Provided parameters would be stored as a list for multiple runs.')
parser.add_argument('--pretrain', type=int, default=0,
                    help='Number of pretraining epochs using the cross entropy loss, so that the learning can always start. Note that it defaults to 100 if dataset==cifar10 and reward<6.1, and the results in the paper are reproduced.')
parser.add_argument('--coverage', type=float, nargs='+',
                    default=[100., 99., 98., 97., 95., 90., 85., 80., 75., 70.,
                             60., 50., 40., 30., 20., 10.],
                    help='the expected coverages used to evaluated the accuracies after abstention')
# Save
parser.add_argument('-s', '--save', default='save', type=str, metavar='PATH',
                    help='path to save checkpoint (default: save)')
parser.add_argument('--loss', default='gambler', type=str,
                    help='loss function')
# Architecture
parser.add_argument('--arch', '-a', metavar='ARCH', default='vgg16_bn',
                    choices=model_names,
                    help='model architecture: ' +
                         ' | '.join(model_names) +
                         ' (default: vgg16_bn) Please edit the code to train with other architectures')
# Miscs
parser.add_argument('--manualSeed', type=int, help='manual seed', default=1)
parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                    help='evaluate trained models on validation set, following the paths defined by "save", "arch" and "rewards"')
# Device options
parser.add_argument('--gpu-id', default='0', type=str,
                    help='id(s) for CUDA_VISIBLE_DEVICES')

args = parser.parse_args()
state = {k: v for k, v in args._get_kwargs()}

# set the abstention definitions
expected_coverage = args.coverage
reward_list = args.rewards

# Use CUDA
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_id
use_cuda = torch.cuda.is_available()

# Random seed
if args.manualSeed is None:
    args.manualSeed = random.randint(1, 10000)
random.seed(args.manualSeed)
torch.manual_seed(args.manualSeed)
if use_cuda:
    torch.cuda.manual_seed_all(args.manualSeed)

num_classes = 10  # this is modified later in main() when defining the specific datasets


def dataset(args):
    # Dataset
    print('==> Preparing dataset %s' % args.dataset)
    global num_classes
    if args.dataset == 'cifar10':
        dataset = dataset_utils.C10
        num_classes = 10
        input_size = 32
        transform_train = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465),
                                 (0.2023, 0.1994, 0.2010))
        ])
        transform_test = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.4914, 0.4822, 0.4465),
                                 (0.2023, 0.1994, 0.2010))
        ])
        trainset = dataset(root=args.data_path + 'CIFAR10', train=True,
                           download=True,
                           transform=transform_train)
        testset = dataset(root=args.data_path + 'CIFAR10', train=False,
                          download=True,
                          transform=transform_test)
    elif args.dataset == 'svhn':
        # dataset = datasets.SVHN
        dataset = dataset_utils.SVHN
        num_classes = 10
        input_size = 32
        transform_train = transforms.Compose([
            transforms.RandomRotation(15),
            transforms.RandomCrop(32, padding=4),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        transform_test = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        trainset = dataset(root=args.data_path + 'SVHN', split='train',
                           download=True,
                           transform=transform_train)
        testset = dataset(root=args.data_path + 'SVHN', split='test',
                          download=True,
                          transform=transform_test)
    elif args.dataset == 'catsdogs':
        num_classes = 2
        input_size = 64
        transform_train = transforms.Compose([
            transforms.RandomCrop(64, padding=6),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])
        transform_test = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])

        # resizing the images to 64 and center crop them, so that they become 64x64 squares
        trainset = dataset_utils.CatsDogs(root=args.data_path + 'cats_dogs',
                                          split='train',
                                          transform=transform_train, resize=64)
        testset = dataset_utils.CatsDogs(root=args.data_path + 'cats_dogs',
                                         split='val', transform=transform_test,
                                         resize=64)

    trainloader = torch.utils.data.DataLoader(trainset,
                                              batch_size=args.train_batch,
                                              shuffle=True,
                                              num_workers=args.workers)
    testloader = torch.utils.data.DataLoader(testset,
                                             batch_size=args.test_batch,
                                             shuffle=False,
                                             num_workers=args.workers)
    return trainloader, testloader


def test(model, device, test_loader):
    model.eval()
    correct = 0
    with torch.no_grad():
        for data, target, _ in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            pred = output.argmax(dim=1,
                                 keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    print(
        '\nTest set: Accuracy: {}/{} ({:.0f}%)\n'.format(
            correct, len(test_loader.dataset),
            100. * correct / len(test_loader.dataset)), flush=True)


def main():
    torch.manual_seed(123)
    device = "cuda:0"

    # create data loaders
    train_loader, test_loader = dataset(args=args)

    # Stage 1: Create DNN
    lower_dataset = args.dataset.lower()
    model_file = f'{lower_dataset}_300.pth'

    if os.path.isfile(model_file):
        print("=> loading checkpoint '{}'".format(model_file), flush=True)
        model = torch.load(model_file)
    else:
        raise Exception(f"Checkpoint file not found: {model_file}.")

    # test(model=model, device=device, test_loader=test_loader)

    # Stage 2: Create OOD detector
    # Fitting is not required in this case

    model.eval()

    for detector_name in [
        'energy',
        'mahalanobis',
        'maxlogit',
        'mcd',
        'odin',
        'softmax',
    ]:
        args.detector = detector_name

        if args.detector == 'energy':
            detector = EnergyBased(model)
        elif args.detector == 'mahalanobis':
            detector = Mahalanobis(model)
            detector.fit(data_loader=test_loader)
        elif args.detector == 'maxlogit':
            detector = MaxLogit(model)
        elif args.detector == 'mcd':
            detector = MCD(model)
        elif args.detector == 'odin':
            detector = ODIN(model)
        elif args.detector == 'softmax':
            detector = MaxSoftmax(model)

        # Stage 3: Evaluate Detectors
        metrics = OODMetrics()

        with torch.no_grad():
            for n, batch in enumerate(test_loader):
                x, y, _ = batch
                x = x.to(device)
                metrics.update(detector(x), y)

        scores = metrics.get_scores()
        print(scores)
        scores = scores.numpy()
        np.save(
            file=f'scores/detector_{args.detector}_dataset_{args.dataset}.npy',
            arr=scores)


if __name__ == "__main__":
    main()
